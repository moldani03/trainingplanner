
import controller.Controller1;
import controller.Controller2;
import page.*;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    public MainFrame() {
        setBounds(100, 100, 500, 500);
        setTitle("Workout planner");
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        CardLayout layout = new CardLayout();
        JPanel panel = new JPanel();
        panel.setLayout(layout);
        add(panel);

        Page3 page3 = new Page3();

        Page1 page1 = new Page1();
        Controller1 controller1 = new Controller1(page1, page3, layout, panel);

        Page2 page2 = new Page2();
        Controller2 controller2 = new Controller2(page1, page2, layout, panel);

        panel.add(page1, "P1");
        panel.add(page2, "P2");
        panel.add(page3, "P3");

        setVisible(true);
    }

    public static void main(String[] args) {
        new MainFrame();
    }
}
