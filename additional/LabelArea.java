package additional;

import javax.swing.*;

public class LabelArea extends JPanel {
    private final JTextArea ta;

    public LabelArea(String str) {
        JLabel label = new JLabel(str);
        add(label);
        ta = new JTextArea();

        ta.setText("<Exersize name> <exersize time>");
        add(ta);
    }

    public JTextArea getTa() {
        return ta;
    }
}
