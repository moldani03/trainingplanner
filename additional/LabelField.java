package additional;

import javax.swing.*;
import java.awt.*;

public class LabelField extends JPanel {
    private final JTextField tf;

    public LabelField(String str) {
        JLabel label = new JLabel(str);
        add(label);
        tf = new JTextField();
        tf.setPreferredSize(new Dimension(150, 30));
        add(tf);
    }

    public JTextField getTf() {
        return tf;
    }
}
