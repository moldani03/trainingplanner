package additional;

import javax.sound.sampled.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WorkoutPlan {
    private static final String storageDir = "PathToStorageDir";
    private final String filePath;
    private int numOfSets;
    private int restBetweenSets;
    private final List<Integer> timeOfEachEx;
    private final List <String> exercises;

    public WorkoutPlan(String filePath){
        this.filePath = filePath;
        timeOfEachEx = new ArrayList<>();
        exercises = new ArrayList<>();
        readFromFile();
    }

    public static String getPath(){
        try(BufferedReader reader = new BufferedReader(new FileReader(storageDir))) {
            return reader.readLine();
        } catch (IOException e) {
            System.out.println("The file containing the path not found: "+ storageDir);
            return null;
        }
    }

    public void readFromFile()  {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            numOfSets = Integer.parseInt(reader.readLine());
            restBetweenSets = Integer.parseInt(reader.readLine());
            String line;
            line = reader.readLine();
            while(line != null){
                int lastIndx = line.lastIndexOf(' ');
                exercises.add(line.substring(0, lastIndx));
                timeOfEachEx.add(Integer.parseInt(line.substring(lastIndx+1)));
                line = reader.readLine();
            }
        } catch (IOException e) {
            System.out.println("Error while reading file: " + filePath);
        }
    }

    public static void playMusic(String path){
        try{
            AudioInputStream ais = AudioSystem.getAudioInputStream(new File(path));
            Clip clip = AudioSystem.getClip();
            clip.open(ais);
            clip.start();
            System.out.println("Music " + path + " started");
        } catch (UnsupportedAudioFileException | IOException e) {
            System.out.println("Error: music file not found: " + path);
        } catch (LineUnavailableException e) {
            System.out.println("Error: LineUnavailableException");
        }
    }

    public int getNumOfSets() {
        return numOfSets;
    }

    public int getRestBetweenSets() {
        return restBetweenSets;
    }

    public List<Integer> getTimeOfEachEx() {
        return timeOfEachEx;
    }

    public List<String> getExercises() {
        return exercises;
    }

    public static String getStorageDir(){
        return storageDir;
    }
}
