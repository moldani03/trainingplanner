package controller;

import additional.WorkoutPlan;
import page.Page1;
import page.Page3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Controller1 {
    private final Page1 page1;
    private static CardLayout layout;
    private static JPanel panel;

    private static Page3 page3;

    public Controller1(Page1 page1, Page3 page3, CardLayout layout, JPanel panel) {
        this.page1 = page1;
        Controller1.page3 = page3;
        Controller1.layout = layout;
        Controller1.panel = panel;

        page1.getbAddDir().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                WorkoutPlan.playMusic("click.wav");
                chooseFolder();
            }
        });
        page1.getbAddWPlan().addActionListener(a -> layout.show(panel, "P2"));
    }

    public void chooseFolder() {
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int result = jfc.showOpenDialog(page1);
        if (result == JFileChooser.APPROVE_OPTION) {
            String folderPath = jfc.getSelectedFile().getAbsolutePath();

            try (BufferedWriter writer = new BufferedWriter(new FileWriter(WorkoutPlan.getStorageDir()))) {
                writer.write(folderPath);
            } catch (IOException e) {
                System.out.println("Error when writing in file");
            }
        }
    }

    public static List<String> getWPlans() { // visszateriti a file-okban lementett workoutplanek neveit
        String filePath = WorkoutPlan.getPath();
        List<String> files = null;
        try {
            assert filePath != null;
            files = Files.walk(Paths.get(filePath), Integer.MAX_VALUE)
                    .filter(Files::isRegularFile)
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            System.out.println("File return error");
        }
        return files;
    }

    public static void addActLToB(JButton b) {
        b.addActionListener(a -> {
            WorkoutPlan.playMusic("song2.wav");
            String fileName = b.getText();
            String path = WorkoutPlan.getPath() + "/" + fileName;
            WorkoutPlan wplan = new WorkoutPlan(path);
            Controller3 c3 = new Controller3(wplan, page3);
            layout.show(panel, "P3");
        });
    }
}