package controller;

import additional.WorkoutPlan;
import page.Page1;
import page.Page2;

import javax.swing.*;
import java.awt.*;
import java.io.*;

public class Controller2 {
    private final Page2 page2;

    public Controller2(Page1 page1, Page2 page2, CardLayout layout, JPanel panel){
        this.page2 = page2;

        page2.getbSave().addActionListener(b -> {
            String wPName = page2.getLf1().getTf().getText();
            if(!wPName.isEmpty()) {
                this.savePlan();
                page1.addWPlan(wPName);
            }
            layout.show(panel, "P1");
        });
        page2.getbCancel().addActionListener(c -> layout.show(panel, "P1"));
    }
    public void savePlan() {
        String path = null;
        try (BufferedReader reader = new BufferedReader(new FileReader(WorkoutPlan.getStorageDir()))) {
            path = reader.readLine();
        } catch (IOException b) {
            System.out.println("File FolderPath not found");
        }
        String WorkoutPlanName = page2.getLf1().getTf().getText();
        String fullPath = path + "/" + WorkoutPlanName;
        System.out.println(fullPath);
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fullPath))) {
            String str = page2.getLf2().getTf().getText();
            writer.write(str + "\n");

            str = page2.getLf3().getTf().getText();
            writer.write(str + "\n");

            str = page2.getLa().getTa().getText();
            writer.write(str);

        } catch (IOException a) {
            System.out.println("file read error");
        }
        // adds a button to page1

    }
}
