package controller;

import additional.WorkoutPlan;
import page.Page3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Controller3 {
    private int currentIndx;
    private int currentTime;
    private int sets;

    public Controller3(WorkoutPlan model, Page3 page3) {
        this.currentIndx = 0;
        this.sets = model.getNumOfSets();

        List<String> exercises = model.getExercises();
        List<Integer> times = model.getTimeOfEachEx();
        this.currentTime = times.get(0);
        int len = exercises.size();

        page3.getlName().setText(exercises.get(currentIndx));
        page3.getlNum().setText(Integer.toString(times.get(currentIndx)));

        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (currentTime > 0) {
                    currentTime -= 1;
                    page3.getlNum().setText(Integer.toString(currentTime));

                } else if (currentIndx < len - 1) {
                    currentIndx++;
                    currentTime = times.get(currentIndx);
                    page3.getlName().setText(exercises.get(currentIndx));
                    page3.getlNum().setText(Integer.toString(times.get(currentIndx)));
                } else if (sets > 0) {
                    sets -= 1;
                    currentTime = model.getRestBetweenSets();
                    page3.getlName().setText("Rest");
                    page3.getlNum().setText(Integer.toString(currentTime));
                    if (sets != 0) {
                        currentIndx = -1;
                    }
                } else {
                    page3.getlName().setText("Nice job");
                }
            }
        });
        timer.start();
    }
}
