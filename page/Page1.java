package page;

import controller.Controller1;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Page1 extends JPanel {
    private final JPanel p1;
    private final List<JButton> bWPlans;
    private final JButton bAddWPlan;
    private final JButton bAddDir;

    public Page1() {
        setLayout(new BorderLayout());
        p1 = new JPanel();
        p1.setLayout(new BoxLayout(p1, BoxLayout.Y_AXIS));

        Font font = new Font("Arial", Font.BOLD, 20);
        JLabel l = new JLabel();
        l.setFont(font);
        l.setText("Workout plans");
        p1.add(l);

        // add buttons
        bWPlans = new ArrayList<>();
        List<String> bNames = Controller1.getWPlans();
        bNames.forEach(this::addWPlan);

        add(p1, BorderLayout.CENTER);

        JPanel p2 = new JPanel();
        bAddWPlan = new JButton("Add Workout Plan");
        p2.add(bAddWPlan);

        bAddDir = new JButton("Choose workout plan directory");
        p2.add(bAddDir);

        add(p2, BorderLayout.SOUTH);
    }

    public void addWPlan(String bName) {
        JButton b = new JButton(bName);
        bWPlans.add(b);
        p1.add(b);
        Controller1.addActLToB(b);
    }

    public JButton getbAddWPlan() {
        return bAddWPlan;
    }

    public JButton getbAddDir() {
        return bAddDir;
    }
}
