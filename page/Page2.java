package page;

import additional.LabelArea;
import additional.LabelField;

import javax.swing.*;

public class Page2 extends JPanel {
    private final LabelField lf1;
    private final LabelField lf2;
    private final LabelField lf3;
    private final LabelArea la;
    private final JButton bSave;
    private final JButton bCancel;

    public Page2() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        lf1 = new LabelField("Workout plan name: ");
        add(lf1);
        lf2 = new LabelField("Number of sets: ");
        add(lf2);

        lf3 = new LabelField("Rest time between sets: ");
        add(lf3);

        la = new LabelArea("Exersizes: ");
        add(la);

        JPanel p = new JPanel();
        bSave = new JButton("Save");
        bCancel = new JButton("Cancel");
        p.add(bCancel);
        p.add(bSave);
        add(p);
    }

    public LabelField getLf1() {
        return lf1;
    }

    public LabelField getLf2() {
        return lf2;
    }

    public LabelField getLf3() {
        return lf3;
    }

    public LabelArea getLa() {
        return la;
    }

    public JButton getbSave() {
        return bSave;
    }

    public JButton getbCancel() {
        return bCancel;
    }
}
