package page;

import javax.swing.*;
import java.awt.*;

public class Page3 extends JPanel {
    private final JLabel lName;
    private final JLabel lNum;

    public Page3() {
        setLayout(new GridLayout(2, 1));

        Font font = new Font("Arial", Font.BOLD, 30);
        lName = new JLabel();
        lName.setFont(font);
        lName.setPreferredSize(new Dimension(200, 100));

        Font fontNum = new Font("Arial", Font.BOLD, 50);
        lNum = new JLabel();
        lNum.setFont(fontNum);
        lNum.setPreferredSize(new Dimension(200, 100));

        JPanel p1 = new JPanel();
        p1.setLayout(new FlowLayout(FlowLayout.CENTER));

        p1.add(lName);
        add(p1);

        JPanel p2 = new JPanel();
        p2.setLayout(new FlowLayout(FlowLayout.CENTER));

        p2.add(lNum);
        add(p2);
    }

    public JLabel getlName() {
        return lName;
    }

    public JLabel getlNum() {
        return lNum;
    }
}
